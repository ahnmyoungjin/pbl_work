package day02;

import java.util.Scanner;

public class IfExam03 {
	

	static void sds(int age) { // 받는 입장에서는 뭐가 들어올지 모르기에 int age 선언
		 if (age >= 20) {
				System.out.println("성인입니다");
			
			}
			else { 
				
				//System.out.println(String.format("당신은 %d년 후에 성인이 됩니다.",20-age));
				System.out.printf("당신은 %d년 후에 성인이 됩니다.",20-age);
			}
	}
	
	
	
	public static void main(String[] args) {
		
		int a;

		Scanner sc = new Scanner(System.in);
		System.out.println("나이 입력");
		a = sc.nextInt();
		
		sds(a);
		
	}

}
