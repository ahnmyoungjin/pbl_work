package day02;

import java.util.Scanner;

public class ifExam07 {
/*
 * 3개의 정수를 입력받아 조건연산자를 이용하여 입력받은 수들 중 최소값을 
 * 출력하는 프로그램을 작성하시오.


입력예:18 -5 10
출력예:-5
 */

	public static void sss(int a,int b,int c) {
		if ((a<b) && (a<c)) System.out.printf("최소값 %d",a);
		else if ((b<a) && (b<c)) System.out.printf("최소값 %d",b);
		else System.out.printf("최소값 %d",c);
	}
	
	
	public static void main(String[] args) {
		System.out.printf("3개 정수 입력");
		Scanner sc = new Scanner(System.in);
		
		int a = sc.nextInt();
		int b = sc.nextInt();
		int c = sc.nextInt();
		sss(a,b,c);
		// TODO Auto-generated method stub

	}

}
