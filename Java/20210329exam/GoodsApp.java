package first;

public class GoodsApp {

//	Goods 객체를 하나 생성하고, 이 객체에 대한 레퍼런스 변수명을 camera로 하세요
//	상품이름(name) 은 "Nikon",
//	값(price)은 400000,
//	재고개수(countStock)은 30
//	팔린개수(countSold)는 50
	
	public static void main(String[] args) {
		
	Goods camera = new Goods();
//	camera.setName("Nikon");
//	camera.setPrice(400000);
//	camera.setCountStock(30);
//	camera.setCountSold(50);
	System.out.println("상품이름:"+camera.getName());
	System.out.println("상품가격:"+camera.getPrice());
	System.out.println("재고수량:"+camera.getCountStock());
	System.out.println("팔린수량:"+camera.getCountSold());
	}

}
