package first;

import java.util.Arrays;

public class ArrayUtils {

	// int 배열을 double 배열로 변환
	static double [] intToDouble( int[] source ) {
		double[] Arr = new double[source.length];
		for (int i=0;i<source.length ;i++) {
			Arr[i]=source[i];
		}
		return Arr;
	}
//	// double 배열을 int 배열로 변환
	static int [] doubleToInt( double[] source ) {
		int[] Arr = new int[source.length];
		for (int i=0;i<source.length;i++) {
			Arr[i]=(int)source[i]; // source가 더블형이기 때문에 int형으로 바꿔줘야한다.
		}
		return Arr;
	}
//	// int 배열 두 개를 연결한 새로운 배열 리턴
	static int [] concat( int[] s1, int[] s2 ) {
		int[] resultArr = new int[s1.length+s2.length];
		int resultIndex = 0;
		for(int i = 0; i < s1.length; i++) {
			resultArr[resultIndex++] = s1[i]; //resultIndex으로 마지막 index 기억?
		}
		for(int i = 0; i < s2.length; i++) {
			resultArr[resultIndex++] = s2[i]; //resultIndex으로 마지막 index 3번 자리부터 시작함
		}
		return resultArr;
	}
	

}