package first;

public class TV {
	private int channel;
	int volume;
	boolean power;
	

	public TV() {
		this(7,20,false);
	}
	public TV(int channel,int volume,boolean power) {
		this.channel=channel;
		this.power=power;
		this.volume=volume;
	}

	public void ChannelUp() {channel++;}
	public void ChannelDown() {channel--;}
	public void volumeUp() {volume++;}
	public void volumeDown() {volume--;}
	public void powerOn() {power = true;}
	public void powerOff() {power = false;}
	public int getChannel() {return channel;}
	public void setChannel(int channel) {this.channel = channel;}
	public boolean isPower() {return power;}
	public int getVolume() {return volume;}
	
	public void show() {
		System.out.printf("TV����(%b) ä��(%d) ����(%d)\n",isPower(),getChannel(),getVolume());
	}
}
