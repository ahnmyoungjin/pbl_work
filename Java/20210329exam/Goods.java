package first;

public class Goods {
	private String name;
	private int price;
	private int countStock;
	private int countSold;
	
	
	public Goods() {  //아무것도 없을때 
		this("nikon",400000,30,50); // 초기값	
		
	}
	
	public Goods(String name, int price, int countStock, int countSold) { //4개를 제대로 넣어져야한다.
	this.name = name;
	this.countSold = countSold;
	this.countStock = countStock;
	this.price = price;
//	this("nikon",400000,30,50);	
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
//		this("Nikon");
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
//		this(400000);
	}
	public int getCountStock() {
		return countStock;
	}
	public void setCountStock(int countStock) {
		this.countStock = countStock;
//		this(30);
	}
	public int getCountSold() {
		return countSold;
	}
	public void setCountSold(int countSold) {
		this.countSold = countSold;
//		this(50);
	}
	public void show() {
		System.out.println("상품이름:"+getName());
		System.out.println("상품가격:"+getPrice());
		System.out.println("재고수량:"+getCountStock());
		System.out.println("팔린수량:"+getCountSold());
		
	}
	
}
