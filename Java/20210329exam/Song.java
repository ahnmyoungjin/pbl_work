package first;


//1) 모든 필드의 getter/setter 메소드를 작성하세요.
//2) 필드는 private 으로 설정하고 getter/setter 에 의해 접근하여야 합니다.
//3) 노래의 정보를 화면에 출력하는 show() 메소드를 작성하세요.
//4) 아이유 의 "좋은날" 노래를 Song 객체로 생성하고 show()를 이용하여 회면에 출력하세요.
//아이유 좋은날 ( Real, 2010, 3번 track, 이민수 작곡 )


public class Song {	
	private String title;
	private String artist;
	private String album;
	private String composer;
	private int year;
	private int track;
	
	public Song() {
		this("좋은날","아이유","Real","이민수",2010,3);
	}
	
	public Song(String title,String artist,String album,String composer,int year,int track) {
		this.title = title;
		this.album = album;
		this.composer = composer;
		this.artist = artist;
		this.year = year;
		this.track = track;
			
	}
//	public Song(String title) {
//		
//	}
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getArtist() {
		return artist;
	}

	public void setArtist(String artist) {
		this.artist = artist;
	}

	public String getAlbum() {
		return album;
	}

	public void setAlbum(String album) {
		this.album = album;
	}

	public String getComposer() {
		return composer;
	}

	public void setComposer(String composer) {
		this.composer = composer;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public int getTrack() {
		return track;
	}

	public void setTrack(int track) {
		this.track = track;
	}

	public void show(){
		
		System.out.printf("%s %s ( %s, %d, %d번 track, %s 작곡 )\n",artist,title,album,year,track,composer);
	}
	
	
	public static void main(String[] args) {
		Song song = new Song();
//		song.setArtist("아이유");
//		song.setComposer("이민수");
//		song.setTitle("좋은날");
//		song.setTrack(3);
//		song.setYear(2010);
//		song.setAlbum("Real");
		song.show();
	}
}
